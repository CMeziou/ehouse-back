### Projet eHouse-back - API de Maison Connectée (Symfony)

## Description
Le projet eHouse-back est une API de maison connectée développée avec Symfony. Cette application permet de contrôler les appareils connectés, d'allumer ou éteindre des dispositifs spécifiques et de surveiller l'état des appareils à distance. Le projet intègre des concepts tels que le diagramme de classe, le diagramme de cas d'utilisation avec StarUML, le maquettage avec Figma, et la gestion du temps avec Trello.

## Fonctionnalités principales
- **Contrôle des appareils :** Permet de contrôler divers appareils connectés à la maison.
- **Allumer/Éteindre :** Permet d'allumer ou d'éteindre des appareils spécifiques à distance.
- **Surveillance des appareils :** Offre une vue d'ensemble des appareils actuellement allumés.
- **Ajouter, supprimer ou modifier un appareil

## Captures d'écran
![diagram de useCase](image.png)
![diagram de calss](image-1.png)
![Wireframes figma](image-2.png)

## Technologies utilisées
- Symfony
- PHP
- MySQL
- StarUML
- Figma
- Trello

## Installation
1. Clonez le dépôt `git clone https://github.com/votre_utilisateur/eHouse-back.git`
2. Installez les dépendances avec Composer `composer install`
3. Configurez la base de données dans le fichier `.env` et exécutez les migrations `php bin/console doctrine:migrations:migrate`

## Auteurs
- Chems MEZIOU
