<?php

namespace App\Controller;

use App\Entities\Device;
use App\Repository\DeviceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route('/api/device')]
class DeviceController extends AbstractController
{
    // Constructor with dependency injection of DeviceRepository
    public function __construct(private DeviceRepository $repo) {}

    /**
     * Get devices in a specific room by room ID.
     * 
     * @param int $id The ID of the room
     * @return JsonResponse A JSON response containing devices in the room
     */
    #[Route('/room/{id}', methods: 'GET')]
    public function deviceByRoom($id): JsonResponse
    {
        return $this->json($this->repo->findDeviceByRoom($id));
    }
    
    /**
     * Get all devices.
     * 
     * @return JsonResponse A JSON response containing all devices
     */
    #[Route(methods: 'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->repo->findAll());
    }

    /**
     * Get a single device by its ID.
     * 
     * @param int $id The ID of the device
     * @return JsonResponse A JSON response containing the device or an error if not found
     */
    #[Route('/{id}', methods: 'GET')]
    public function one($id): JsonResponse
    {
        return $this->json($this->repo->findById($id));
    }

    /**
     * Create a new device.
     * 
     * @param Request $request The HTTP request
     * @param SerializerInterface $serializer The serializer to deserialize the request
     * @return JsonResponse A JSON response containing the created device or validation errors
     */
    #[Route(methods: 'POST')]
    public function create(Request $request, SerializerInterface $serializer): JsonResponse
    {
        try {
            // Deserialize request content into a Device object
            $device = $serializer->deserialize($request->getContent(), Device::class, 'json');
            $this->repo->persist($device);

            return $this->json($device, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            // Handle validation errors
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            // Handle invalid JSON format
            return $this->json('Invalid JSON', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update an existing device by its ID.
     * 
     * @param int $id The ID of the device to update
     * @param Request $request The HTTP request
     * @param SerializerInterface $serializer The serializer to deserialize the request
     * @return JsonResponse A JSON response containing the updated device or validation errors
     * @throws NotFoundHttpException If the device is not found
     */
    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer): JsonResponse
    {
        // Find the existing device by ID
        $device = $this->repo->findById($id);
        if (!$device) {
            throw new NotFoundHttpException();
        }

        try {
            // Deserialize request content into a Device object
            $toUpdate = $serializer->deserialize($request->getContent(), Device::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);

            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            // Handle validation errors
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            // Handle invalid JSON format
            return $this->json('Invalid JSON', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a device by its ID.
     * 
     * @param int $id The ID of the device to delete
     * @return JsonResponse A JSON response indicating success or an error if the device is not found
     */
    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id): JsonResponse
    {
        // Find the existing device by ID
        $device = $this->repo->findById($id);

        if ($device === null) {
            // Handle device not found
            return $this->json(['message' => 'Device not found'], 404);
        }

        // Delete the device
        $this->repo->delete($device);

        return $this->json(['message' => 'Device deleted']);
    }
}
