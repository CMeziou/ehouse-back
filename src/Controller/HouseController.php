<?php

namespace App\Controller;

use App\Entities\House;
use App\Entities\Room;
use App\Repository\HouseRepository;
use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/house')]
class HouseController extends AbstractController
{
    // Constructor with dependency injection of HouseRepository and RoomRepository
    public function __construct(private HouseRepository $houseRepo, private RoomRepository $roomRepo) {}

    /**
     * Get all houses.
     * 
     * @return JsonResponse A JSON response containing all houses
     */
    #[Route(methods: 'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->houseRepo->findAll());
    }

    /**
     * Get a single house by its ID.
     * 
     * @param int $id The ID of the house
     * @return JsonResponse A JSON response containing the house or an error if not found
     */
    #[Route('/{id}', methods: 'GET')]
    public function getOne(int $id): JsonResponse
    {
        $house = $this->houseRepo->findById($id);

        if ($house === null) {
            return $this->json(['message' => 'House not found'], 404);
        }

        return $this->json($house);
    }

    /**
     * Create a new house.
     * 
     * @param Request $request The HTTP request
     * @param SerializerInterface $serializer The serializer to deserialize the request
     * @return JsonResponse A JSON response containing the created house or validation errors
     */
    #[Route(methods: 'POST')]
    public function create(Request $request, SerializerInterface $serializer): JsonResponse
    {
        try {
            // Deserialize request content into a House object
            $house = $serializer->deserialize($request->getContent(), House::class, 'json');
            $this->houseRepo->persist($house);

            return $this->json($house, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            // Handle validation errors
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            // Handle invalid JSON format
            return $this->json('Invalid JSON', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a new room within a house.
     * 
     * @param Request $request The HTTP request
     * @param int $id The ID of the house to which the room will be added
     * @param SerializerInterface $serializer The serializer to deserialize the request
     * @param ValidatorInterface $validator The validator to validate the Room object
     * @return JsonResponse A JSON response containing the created room or validation errors
     */
    #[Route('/{id}', methods: 'POST')]
    public function createRoom(Request $request, int $id, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        $house = $this->houseRepo->findById($id);

        if ($house === null) {
            return $this->json(['message' => 'House not found'], 404);
        }

        // Deserialize request content into a Room object
        $room = $serializer->deserialize($request->getContent(), Room::class, 'json');

        // Validate the Room object
        $errors = $validator->validate($room);
        if (count($errors) > 0) {
            return $this->json(['message' => 'Validation error', 'errors' => $errors], 400);
        }

        // Set the house for the room and persist it
        $room->setHouse($house);
        $this->roomRepo->persist($room);

        return $this->json($room, 201);
    }

    /**
     * Update a house by its ID.
     * 
     * @param Request $request The HTTP request
     * @param int $id The ID of the house to update
     * @return JsonResponse A JSON response containing the updated house or an error if not found
     */
    #[Route('/{id}', methods: 'PUT')]
    public function update(Request $request, int $id): JsonResponse
    {
        $house = $this->houseRepo->findById($id);

        if ($house === null) {
            return $this->json(['message' => 'House not found'], 404);
        }

        // Decode JSON request data
        $data = json_decode($request->getContent(), true);

        // Check if 'name' is provided in the request data
        if (!isset($data['name'])) {
            return $this->json(['message' => 'Name is required'], 400);
        }

        // Update the house name and persist it
        $house->setName($data['name']);
        $this->houseRepo->update($house);

        return $this->json($house);
    }

    /**
     * Delete a house by its ID.
     * 
     * @param int $id The ID of the house to delete
     * @return JsonResponse A JSON response indicating success or an error if the house is not found
     */
    #[Route('/{id}', methods: 'DELETE')]
    public function delete(int $id): JsonResponse
    {
        $house = $this->houseRepo->findById($id);

        if ($house === null) {
            return $this->json(['message' => 'House not found'], 404);
        }

        // Delete the house
        $this->houseRepo->delete($id);

        return $this->json(['message' => 'House deleted']);
    }
}
