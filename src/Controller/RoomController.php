<?php

namespace App\Controller;

use App\Entities\Device;
use App\Entities\Room;
use App\Repository\DeviceRepository;
use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/room')]
class RoomController extends AbstractController
{
    // Constructor with dependency injection of RoomRepository and DeviceRepository
    public function __construct(private RoomRepository $repo, private DeviceRepository $repoDevice) {}

    /**
     * Get all rooms.
     * 
     * @return JsonResponse A JSON response containing all rooms
     */
    #[Route(methods: 'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->repo->findAll());
    }

    /**
     * Get rooms in a specific house by house ID.
     * 
     * @param int $id The ID of the house
     * @return JsonResponse A JSON response containing the rooms in the house or an error if the house is not found
     */
    #[Route('/house/{id}', methods: 'GET')]
    public function roomByHouse($id): JsonResponse
    {
        return $this->json($this->repo->findRoomByHouse($id));
    }

    /**
     * Get a single room by its ID.
     * 
     * @param int $id The ID of the room
     * @return JsonResponse A JSON response containing the room or an error if the room is not found
     */
    #[Route('/{id}', methods: 'GET')]
    public function getById(int $id): JsonResponse
    {
        $room = $this->repo->findById($id);

        if ($room === null) {
            return $this->json(['message' => 'Room not found'], 404);
        }

        return $this->json($room);
    }

    /**
     * Create a new room.
     * 
     * @param Request $request The HTTP request
     * @param SerializerInterface $serializer The serializer to deserialize the request
     * @return JsonResponse A JSON response containing the created room or validation errors
     */
    #[Route(methods: 'POST')]
    public function persiste(Request $request, SerializerInterface $serializer): JsonResponse
    {
        try {
            // Deserialize request content into a Room object
            $room = $serializer->deserialize($request->getContent(), Room::class, 'json');
            $this->repo->persist($room);

            return $this->json($room, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            // Handle validation errors
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            // Handle invalid JSON format
            return $this->json('Invalid JSON', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a new device within a room by room ID.
     * 
     * @param Request $request The HTTP request
     * @param int $id The ID of the room to which the device will be added
     * @param SerializerInterface $serializer The serializer to deserialize the request
     * @param ValidatorInterface $validator The validator to validate the Device object
     * @return JsonResponse A JSON response containing the created device or validation errors
     */
    #[Route('/{id}', methods: 'POST')]
    public function createDevice(Request $request, int $id, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        $room = $this->repo->findById($id);

        if ($room === null) {
            return $this->json(['message' => 'Room not found'], 404);
        }

        // Deserialize request content into a Device object
        $device = $serializer->deserialize($request->getContent(), Device::class, 'json');

        // Validate the Device object
        $errors = $validator->validate($device);
        if (count($errors) > 0) {
            return $this->json(['message' => 'Validation error', 'errors' => $errors], 400);
        }

        // Set the room for the device and persist it
        $device->setRoom($room);
        $this->repoDevice->persist($device);

        return $this->json($device, 201);
    }

    /**
     * Update a room by its ID.
     * 
     * @param Request $request The HTTP request
     * @param int $id The ID of the room to update
     * @return JsonResponse A JSON response containing the updated room or an error if the room is not found
     */
    #[Route('/{id}', methods: 'PUT')]
    public function update(Request $request, int $id): JsonResponse
    {
        $room = $this->repo->findById($id);

        if ($room === null) {
            return $this->json(['message' => 'Room not found'], 404);
        }

        // Decode JSON request data
        $data = json_decode($request->getContent(), true);

        // Check if 'name' and 'created_at' are provided in the request data
        if (!isset($data['name']) || !isset($data['created_at'])) {
            return $this->json(['message' => 'Name and created_at are required'], 400);
        }

        // Update the room name and created_at and persist it
        $room->setName($data['name']);
        $room->setCreatedAt(new \DateTime($data['created_at']));
        $this->repo->update($room);

        return $this->json($room);
    }

    /**
     * Delete a room by its ID.
     * 
     * @param int $id The ID of the room to delete
     * @return JsonResponse A JSON response indicating success or an error if the room is not found
     */
    #[Route('/{id}', methods: 'DELETE')]
    public function delete(int $id): JsonResponse
    {
        $room = $this->repo->findById($id);

        if ($room === null) {
            return $this->json(['message' => 'Room not found'], 404);
        }

        // Delete the room
        $this->repo->delete($room);

        return $this->json(['message' => 'Room deleted']);
    }
}
