<?php

namespace App\Entities;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

class Device{
    private ?int $id;

    #[Assert\NotBlank]
    private ?string $name;

    #[Assert\NotBlank]
    private ?string $type;

    #[Assert\NotBlank]
    private ?string $status;

    #[Assert\NotBlank]
    private ?DateTime $createdAt;

    private Room $room;

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getName(): ?string {
		return $this->name;
	}
	
	/**
	 * @param  $name 
	 * @return self
	 */
	public function setName(?string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getType(): ?string {
		return $this->type;
	}
	
	/**
	 * @param  $type 
	 * @return self
	 */
	public function setType(?string $type): self {
		$this->type = $type;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getStatus(): ?string {
		return $this->status;
	}
	
	/**
	 * @param  $status 
	 * @return self
	 */
	public function setStatus(?string $status): self {
		$this->status = $status;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getCreatedAt(): ?DateTime {
		return $this->createdAt;
	}
	
	/**
	 * @param  $created_at 
	 * @return self
	 */
	public function setCreatedAt(?DateTime $created_at): self {
		$this->createdAt = $created_at;
		return $this;
	}
	
	/**
	 * @return Room
	 */
	public function getRoom(): Room {
		return $this->room;
	}
	
	/**
	 * @param Room $room 
	 * @return self
	 */
	public function setRoom(Room $room): self {
		$this->room = $room;
		return $this;
	}
}