<?php

namespace App\Entities;
use Symfony\Component\Validator\Constraints as Assert;

use DateTime;

class Room{
    private ?int $id;
    #[Assert\NotBlank]
    private ?string $name;
    
    #[Assert\NotBlank]
    private ?DateTime $createdAt;

	private House $house;
	

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getName(): ?string {
		return $this->name;
	}
	
	/**
	 * @param  $name 
	 * @return self
	 */
	public function setName(?string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getCreatedAt(): ?DateTime {
		return $this->createdAt;
	}
	
	/**
	 * @param  $createdAt 
	 * @return self
	 */
	public function setCreatedAt(?DateTime $createdAt): self {
		$this->createdAt = $createdAt;
		return $this;
	}



	/**
	 * @return House
	 */
	public function getHouse(): House {
		return $this->house;
	}
	
	/**
	 * @param House $house 
	 * @return self
	 */
	public function setHouse(House $house): self {
		$this->house = $house;
		return $this;
	}
}