<?php

namespace App\Repository;

use PDO;

class Connection
{
    // Static method to establish a PDO database connection
    public static function getConnection()
    {
        // Create a new PDO connection using environment variables for configuration
        // Replace placeholders with actual environment variable names (e.g., DB_HOST, DB_NAME, DB_USER, DB_PASSWORD)
        $pdo = new PDO(
            "mysql:host={$_ENV['DB_HOST']};dbname={$_ENV['DB_NAME']}",
            $_ENV['DB_USER'],
            $_ENV['DB_PASSWORD']
        );

        // Return the PDO connection
        return $pdo;
    }
}

