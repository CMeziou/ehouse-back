<?php

namespace App\Repository;

use App\Entities\Device;
use App\Entities\Room;
use PDO;

class DeviceRepository
{
    /**
     * Find devices by room ID.
     * 
     * @param int $idRoom The ID of the room to filter devices by
     * @return Device[] An array of Device entities that belong to the specified room
     */
    public function findDeviceByRoom(int $idRoom): array
    {
        $list = [];
        $connection = Connection::getConnection();
        $query = $connection->prepare('SELECT d.*, r.name as room_name FROM device d LEFT JOIN room r ON d.id_room = r.id WHERE d.id_room=:id_room');
        $query->bindValue(':id_room', $idRoom);
        $query->execute();

        foreach ($query->fetchAll() as $line) {
            $device = new Device();
            $device->setId($line['id']);
            $device->setName($line['name']);
            $device->setType($line['type']);
            $device->setStatus($line['status']);
            $device->setCreatedAt(new \DateTime($line['created_at']));

            $room = new Room();
            $room->setId($line['id_room']);
            $room->setName($line['room_name']);
            $room->setCreatedAt(new \DateTime($line['created_at']));
            $device->setRoom($room);

            $list[] = $device;
        }

        return $list;
    }

    /**
     * Find all devices.
     * 
     * @return Device[] An array of all Device entities
     */
    public function findAll(): array
    {
        $list = [];
        $connection = Connection::getConnection();
        $query = $connection->prepare('SELECT d.*, d.id id_devices, r.id id_room, r.name as room_name FROM device d LEFT JOIN room r ON d.id_room = r.id');
        $query->execute();

        foreach ($query->fetchAll() as $line) {
            $device = new Device();
            $device->setId($line['id_devices']);
            $device->setName($line['name']);
            $device->setType($line['type']);
            $device->setStatus($line['status']);
            $device->setCreatedAt(new \DateTime($line['created_at']));

            $room = new Room();
            $room->setId($line['id_room']);
            $room->setName($line['room_name']);
            $room->setCreatedAt(new \DateTime($line['created_at']));
            $device->setRoom($room);

            $list[] = $device;
        }

        return $list;
    }

    /**
     * Find a device by its ID.
     * 
     * @param int $id The ID of the device to find
     * @return Device|null The found device or null if not found
     */
    public function findById(int $id): ?Device
    {
        $connection = Connection::getConnection();
        $query = $connection->prepare('SELECT * FROM device WHERE id = :id');
        $query->bindValue(':id', $id);
        $query->execute();
        $line = $query->fetch();

        if ($line) {
            $device = new Device();
            $device->setId($line['id']);
            $device->setName($line['name']);
            $device->setType($line['type']);
            $device->setStatus($line['status']);
            $device->setCreatedAt(new \DateTime($line['created_at']));
            return $device;
        }

        return null;
    }

    /**
     * Persist (create) a new device.
     * 
     * @param Device $device The device to be created and persisted
     * @return void
     */
    public function persist(Device $device): void
    {
        $connection = Connection::getConnection();
        $query = $connection->prepare('INSERT INTO device (name, type, status, created_at, id_room) VALUES (:name, :type, :status, :created_at, :id_room)');
        $query->bindValue(':name', $device->getName());
        $query->bindValue(':type', $device->getType());
        $query->bindValue(':status', $device->getStatus());
        $query->bindValue(':created_at', $device->getCreatedAt()->format('Y-m-d H:i:s'));
        $query->bindValue(':id_room', $device->getRoom()->getId());
        $query->execute();

        $device->setId($connection->lastInsertId());
    }

    /**
     * Update an existing device.
     * 
     * @param Device $device The device to be updated
     * @return void
     */
    public function update(Device $device)
    {
        $connection = Connection::getConnection();
        $query = $connection->prepare('UPDATE device SET name=:name,type=:type,status=:status,created_at=:created_at,id_room=:id_room WHERE id=:id');
        $query->bindValue(':id', $device->getId());
        $query->bindValue(':name', $device->getName());
        $query->bindValue(':type', $device->getType());
        $query->bindValue(':status', $device->getStatus());
        $query->bindValue(':created_at', $device->getCreatedAt()->format('Y-m-d H:i:s'));
        $query->bindValue(':id_room', $device->getRoom()->getId());
        $query->execute();

        $device->setId($connection->lastInsertId());
    }

    /**
     * Delete a device.
     * 
     * @param Device $device The device to be deleted
     * @return void
     */
    public function delete(Device $device)
    {
        $connection = Connection::getConnection();
        $statement = $connection->prepare('DELETE FROM device WHERE id=:id');
        $statement->bindValue('id', $device->getId(), PDO::PARAM_INT);
        $statement->execute();
    }
}
