<?php

namespace App\Repository;

use App\Entities\House;
use PDO;

class HouseRepository
{
    private PDO $connection;

    /**
     * Constructor for HouseRepository.
     * 
     * Initializes the PDO connection.
     */
    public function __construct()
    {
        $this->connection = Connection::getConnection();
    }

    /**
     * Find all houses.
     * 
     * @return House[] An array of House entities
     */
    public function findAll(): array
    {
        $list = [];
        $query = $this->connection->prepare('SELECT * FROM house');
        $query->execute();

        foreach ($query->fetchAll() as $line) {
            $house = new House();
            $house->setName($line['name']);
            $house->setId($line['id']);
            $list[] = $house;
        }

        return $list;
    }

    /**
     * Find a house by its ID.
     * 
     * @param int $id The ID of the house to find
     * @return House|null The found house or null if not found
     */
    public function findById(int $id): ?House
    {
        $query = $this->connection->prepare('SELECT * FROM house WHERE id = :id');
        $query->bindValue(':id', $id);
        $query->execute();
        $line = $query->fetch();

        if ($line) {
            $house = new House();
            $house->setId($line['id']);
            $house->setName($line['name']);
            return $house;
        }

        return null;
    }

    /**
     * Persist (create) a new house.
     * 
     * @param House $house The house to be created and persisted
     * @return void
     */
    public function persist(House $house): void
    {
        $query = $this->connection->prepare('INSERT INTO house (name) VALUES (:name)');
        $query->bindValue(':name', $house->getName());
        $query->execute();

        $house->setId($this->connection->lastInsertId());
    }

    /**
     * Update an existing house.
     * 
     * @param House $house The house to be updated
     * @return void
     */
    public function update(House $house): void
    {
        $query = $this->connection->prepare('UPDATE house SET name = :name WHERE id = :id');
        $query->bindValue(':name', $house->getName());
        $query->bindValue(':id', $house->getId());
        $query->execute();
    }

    /**
     * Delete a house by its ID.
     * 
     * @param int $id The ID of the house to be deleted
     * @return void
     */
    public function delete(int $id): void
    {
        $query = $this->connection->prepare('DELETE FROM house WHERE id = :id');
        $query->bindValue(':id', $id);
        $query->execute();
    }
}
