<?php

namespace App\Repository;

use App\Entities\Device;
use App\Entities\House;
use App\Entities\Room;
use PDO;

class RoomRepository
{
    /**
     * Find all rooms.
     * 
     * @return Room[] An array of all Room entities
     */
    public function findAll(): array
    {
        $list = [];
        $connection = Connection::getConnection();
        $query = $connection->prepare('SELECT * FROM room');
        $query->execute();
        foreach ($query->fetchAll() as $line) {
            $room = new Room();
            $room->setId($line['id']);
            $room->setName($line['name']);
            $room->setCreatedAt(new \DateTime($line['created_at']));
            $list[] = $room;

            $house = new House();
            $house->setId($line['id_house']);
            $house->setName($line['name']);
            $room->setHouse($house);
        }

        return $list;
    }

    /**
     * Find rooms by house ID.
     * 
     * @param int $idHouse The ID of the house to filter rooms by
     * @return Room[] An array of Room entities that belong to the specified house
     */
    public function findRoomByHouse(int $idHouse): array
    {
        $list = [];
        $connection = Connection::getConnection();
        $query = $connection->prepare('SELECT r.*, h.name as house_name FROM room r LEFT JOIN house h ON r.id_house = h.id WHERE r.id_house=:id_house');
        $query->bindValue(':id_house', $idHouse);
        $query->execute();
        
        foreach ($query->fetchAll() as $line) {
            $room = new Room();
            $room->setId($line['id']);
            $room->setName($line['name']);
            $room->setCreatedAt(new \DateTime($line['created_at']));
            
            $house = new House();
            $house->setId($line['id_house']);
            $house->setName($line['house_name']);
            $room->setHouse($house);
            
            $list[] = $room;
        }

        return $list;
    }

    /**
     * Find a room by its ID.
     * 
     * @param int $id The ID of the room to find
     * @return Room|null The found room or null if not found
     */
    public function findById(int $id): ?Room
    {
        $connection = Connection::getConnection();
        $query = $connection->prepare('SELECT * FROM room WHERE id = :id');
        $query->bindValue(':id', $id);
        $query->execute();
        $line = $query->fetch();

        if ($line) {
            $room = new Room();
            $room->setId($line['id']);
            $room->setName($line['name']);
            $room->setCreatedAt(new \DateTime($line['created_at']));
            return $room;
        }

        return null;
    }

    /**
     * Persist (create) a new room.
     * 
     * @param Room $room The room to be created and persisted
     * @return void
     */
    public function persist(Room $room): void
    {
        $connection = Connection::getConnection();
        $query = $connection->prepare('INSERT INTO room (name, created_at, id_house) VALUES (:name, :created_at, :id_house)');
        $query->bindValue(':name', $room->getName());
        $query->bindValue(':created_at', $room->getCreatedAt()->format('Y-m-d'));
        $query->bindValue(':id_house', $room->getHouse()->getId());
        $query->execute();

        $room->setId($connection->lastInsertId());
    }

    /**
     * Create a new device for a room.
     * 
     * @param Room $room The room where the device belongs
     * @param Device $device The device to be created and associated with the room
     * @return void
     */
    public function createDevice(Room $room, Device $device): void
    {
        $connection = Connection::getConnection();
        $query = $connection->prepare('INSERT INTO device (name, type, status, created_at, id_room) VALUES (:name, :type, :status, :created_at, :id_room)');
        $query->bindValue(':name', $device->getName());
        $query->bindValue(':type', $device->getType());
        $query->bindValue(':status', $device->getStatus());
        $query->bindValue(':created_at', $device->getCreatedAt());
        $query->bindValue(':id_room', $room->getId());
        $query->execute();

        $room->setId($connection->lastInsertId());
    }

    /**
     * Update an existing room.
     * 
     * @param Room $room The room to be updated
     * @return void
     */
    public function update(Room $room): void
    {
        $connection = Connection::getConnection();
        $query = $connection->prepare('UPDATE room SET name = :name, createdAt = :created_at WHERE id = :id');
        $query->bindValue(':id', $room->getId());
        $query->bindValue(':name', $room->getName());
        $query->bindValue(':created_at', $room->getCreatedAt());
        $query->execute();
    }

    /**
     * Delete a room.
     * 
     * @param Room $room The room to be deleted
     * @return void
     */
    public function delete(Room $room): void
    {
        $connection = Connection::getConnection();
        $query = $connection->prepare('DELETE FROM room WHERE id = :id');
        $query->bindValue('id', $room->getId(), PDO::PARAM_INT);
        $query->execute();
    }
}
