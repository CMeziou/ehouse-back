<?php

namespace App\Test;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HouseTest extends WebTestCase{
    
public function test(){
    $client = static::createClient();

    $client->request('GET', '/api/house');

    $this->assertResponseIsSuccessful();

    $body = json_decode($client->getResponse()->getContent(), true);

    $item = $body[0];

    $this->assertArrayHasKey('id', $item);
        $this->assertIsInt($item['id']);
        $this->assertIsString($item['name']);

    // $this->assertCount(3, $body);
    // $item = $body[3];
    // $this->assertIsInt($item['id']);
    // $this->assertIsString($item['name']);
  
} 

} 
